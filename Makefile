#
# Copyright (C) 2012-2019 Red Hat, Inc.
#   This copyrighted material is made available to anyone wishing to use,
#  modify, copy, or redistribute it subject to the terms and conditions of
#  the GNU General Public License v.2.
#
#   This application is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#   General Public License for more details.
#
# Authors:
#   Guy Streeter <guy.streeter@gmail.com>
#

PACKAGE=libnuma
SHELL=/bin/bash
PYTHON=python3

SPECFILE=$(PYTHON)-$(PACKAGE).spec

VERSION := $(shell rpm -q --qf '%{VERSION} ' --specfile $(SPECFILE) | cut -d' ' -f1)
RELEASE := $(shell rpmspec -q --qf '%{RELEASE}' --srpm $(SPECFILE) | sed -e 's/\.centos$$//' -e 's/\.[^.]*$$//')

ifndef VERSION
version=$(shell sed -n '/^Version:\s\+/s/^Version:\s\+\(.[0-9]\)/\1/p' < $(SPECFILE))
VERSION=$(shell /bin/echo $(version))
endif
ifndef VERSION
$(error VERSION must be set!)
endif

RPMDIRS=$(addprefix rpm/, SOURCES BUILD RPMS SRPMS)
RPM_TARBALL=rpm/SOURCES/python-libnuma-$(VERSION)-$(RELEASE).tar.gz

DEB_BUILDDIR=deb_builddir
DEB_TARBALL=python-libnuma_$(VERSION).orig.tar.gz
DEB_SRCDIR=$(DEB_BUILDDIR)/python-libnuma-$(VERSION)

PYTHONVERSION=$(shell $(PYTHON) -c 'import sys;print("%d.%d"%(sys.version_info[0],sys.version_info[1]))')
space :=
space +=
PYTHONPATH=$(subst $(space),:,$(wildcard build/lib*$(PYTHONVERSION)*))

all: translations build

build:
	$(PYTHON) ./setup.py build_ext

tests:
	@echo EXPECT SOME STDERR OUTPUT
	@echo
	PYTHONPATH=$(PYTHONPATH) $(PYTHON) -m unittest discover -s src -p '*tests.py'

$(RPMDIRS):
	mkdir -p $@ || :

$(RPM_TARBALL): $(RPMDIRS)
	git archive --format=tar --prefix=python-libnuma-$(VERSION)-$(RELEASE)/ HEAD | gzip -9 > $@

tarball: $(RPM_TARBALL)

$(DEB_BUILDDIR):
	mkdir -p $@

$(DEB_BUILDDIR)/$(DEB_TARBALL): $(DEB_BUILDDIR)
	git archive --format=tar --prefix=python-libnuma-$(VERSION)/ HEAD | gzip -9 > $(DEB_BUILDDIR)/$(DEB_TARBALL)

deb-tarball: $(DEB_BUILDDIR)/$(DEB_TARBALL)

deb: $(DEB_SRCDIR) deb-tarball
	cd $(DEB_SRCDIR); dpkg-buildpackage -us -uc

$(DEB_SRCDIR): $(DEB_BUILDDIR)/$(DEB_TARBALL)
	cd $(DEB_BUILDDIR); tar -xf $(DEB_TARBALL)

rpm: $(RPMDIRS) rpm/SOURCES/python-libnuma-$(VERSION)-$(RELEASE).tar.gz $(SPECFILE)
	rpmbuild -ba --define "_topdir $(PWD)/rpm" --define "_source_filedigest_algorithm 1" --define "_binary_filedigest_algorithm 1" --define "_binary_payload w9.gzdio" --define "_source_payload w9.gzdio" $(SPECFILE)

srpm: $(RPMDIRS) rpm/SOURCES/python-libnuma-$(VERSION)-$(RELEASE).tar.gz $(SPECFILE)
	rpmbuild -bs --define "_topdir $(PWD)/rpm" --define "_source_filedigest_algorithm 1" --define "_binary_filedigest_algorithm 1" --define "_binary_payload w9.gzdio" --define "_source_payload w9.gzdio" $(SPECFILE)

clean:
	rm -rf rpm/{SOURCES,SRPMS,BUILD,RPMS}/* python*-libnuma.lang src/libnuma.c src/libnuma*.so build dist python*_libnuma.egg-info $(DEB_BUILDDIR)
	find . -type f \( -name \*~ -o -name \*.pyc \) -delete
	find translations/locale -type f -name python\*-libnuma.mo -delete

translations/messages.pot: src/libnuma.pyx
	$(eval TMP := $(shell mktemp))
	$(PYTHON) setup.py extract_messages -o $(TMP)
	sed -e "s/FIRST AUTHOR <EMAIL@ADDRESS>/Guy Streeter <guy.streeter@gmail.com>/" <$(TMP) >"$@"
	rm $(TMP)

translations/locale/en_US/LC_MESSAGES/$(PYTHON)-libnuma.po: translations/messages.pot
	$(PYTHON) setup.py init_catalog

translations/locale/en_US/LC_MESSAGES/$(PYTHON)-libnuma.mo: translations/locale/en_US/LC_MESSAGES/$(PYTHON)-libnuma.po
	$(PYTHON) setup.py compile_catalog

translations: translations/locale/en_US/LC_MESSAGES/$(PYTHON)-libnuma.mo

git-tag:
ifndef RELEASE
$(error RELEASE must be set!)
endif
	git tag -s $(VERSION)-$(RELEASE)
