.. include:: <isonum.txt>

===============================
Python Bindings For ``libnuma``
===============================

    Copyright |copy| 2018-202 Guy Streeter ``<guy.streeter@gmail.com>``

See the tests directory in the source reposirity for example usage.

To install on Fedora:

.. code-block:: bash

    dnf copr enable streeter/python-hwloc
    dnf install python3-libnuma

For other RPM-based systems:

.. code-block:: bash

    dnf install git gcc python3-devel python3-setuptools python3-Cython python3-babel
    git clone https://gitlab.com/guystreeter/python-libnuma.git
    cd python-libnuma
    make rpm

Then install the RPM file in the rpm/RPMS folder.

For DEB-based systems:

.. code-block:: bash

    sudo apt-get update
    sudo apt-get install git build-essential libpython3.6-dev \
        debhelper dh-python python-all python-setuptools python3-all \
        python3-setuptools cython cython3 libnuma-dev python-babel python3-babel
    git clone https://gitlab.com/guystreeter/python-libnuma.git
    cd python-libnuma
    make deb

Then install the .deb package in the deb_builddir folder.
