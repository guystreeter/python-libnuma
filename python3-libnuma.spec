#
# Copyright (C) 2012-2021 Guy Streeter
#   This copyrighted material is made available to anyone wishing to use,
#  modify, copy, or redistribute it subject to the terms and conditions of
#  the GNU General Public License v.2.
#
#   This application is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#   General Public License for more details.
#
# Authors:
#   Guy Streeter <guy.streeter@gmail.com>
#

%if 0%{?rhel} && 0%{?rhel} <= 7
%{error: RHEL < 8 is not supported}
%endif

%global _numactl_version 2.0

Name:           python3-libnuma
Version:        3.1
Release:        %{_numactl_version}%{?dist}
Summary:        Python bindings for libnuma

Group:          Development/Languages
License:        GPLv2+
URL:            https://gitlab.com/guystreeter/python-libnuma
Source0:        python-libnuma-%{version}-%{_numactl_version}.tar.gz

BuildRequires:  gcc
BuildRequires:  python3, python3-devel, python3-setuptools, python3-Cython, python3-babel
BuildRequires:  numactl-devel >= %{_numactl_version}

ExcludeArch: s390 s390x %{arm}

%global source_date_epoch_from_changelog 1
%global debug_package %{nil}
%{!?_licensedir:%global license %doc}
%global _privatelibs libnuma
%global __provides_exclude ^(%{_privatelibs})\\.so

%description
Python 3 bindings for libnuma

%prep
%setup -q -n python-libnuma-%{version}-%{_numactl_version}


%build
%{__python3} setup.py build
strip build/*/libnuma*.so
chmod 0755 build/*/libnuma*.so

%install
LICENSEDIR=%{_licensedir}/python3-libnuma
export LICENSEDIR
%{__python3} setup.py install --skip-build --root %{buildroot}
%find_lang python3-libnuma
%check
make PYTHON=python3 tests

%files -f python3-libnuma.lang
%license COPYING
%license LICENSE
%{python3_sitearch}/*


%changelog
* Tue Jan 12 2021 Guy Streeter <guy.streeter@gmail.com> - 3.1-2.0
- Correct bitmap size

* Wed Apr 29 2020 Guy Streeter <guy.streeter@gmail.com> - 3-2.0
- Build only for Python 3

* Tue Aug 13 2019 Guy Streeter <guy.streeter@gmail.com> - 2.3.8-2.0
- build for epel7

* Thu May  2 2019 Guy Streeter <guy.streeter@gmail.com> - 2.3.6-2.0
- require python2-Cython instead of Cython

* Wed Feb 27 2019 Guy Streeter <guy.streeter@gmail.com> - 2.3.5-2.0
- No changes to the RPM build. Added support for Ubuntu packaging

* Tue Oct 30 2018 Guy Streeter <guy.streeter@gmail.com> - 2.3.4-2.0
- build-req gcc
- let the Makefile figure out the PYTHONPATH for make check

* Mon Mar 19 2018 Guy Streeter <guy.streeter@gmail.com> - 2.3.3-2.0
- misc build cleanup

* Wed Jan  3 2018 Guy Streeter <guy.streeter@gmail.com> - 2.3.2-2.0
- really final pip final fixes finally

* Mon Jan  1 2018 Guy Streeter <guy.streeter@gmail.com> - 2.3.1-2.0
- completed support for pip install

* Fri Dec 22 2017 Guy Streeter <guy.streeter@gmail.com> - 2.3-2.0
- move setup.py to the top level, for pip install

* Mon Oct  9 2017 Guy Streeter <guy.streeter@gmail.com> - 2.2.7-2.0
- Copyright, Author, and URL updates.
- PEP code style changes

* Wed Dec 23 2015 Guy Streeter <streeter@redhat.com> - 2.2.6-2.0
- support RHEL 7. (RHEL6 Cython is too old)

* Wed Dec  2 2015 Guy Streeter <streeter@redhat.com> - 2.2.5.2-2.0
- really make sure %%license works in F23

* Fri Nov  6 2015 Guy Streeter <streeter@redhat.com> - 2.2.5.1-2.0
- make sure %%license works in F23

* Fri Nov  6 2015 Guy Streeter <streeter@redhat.com> - 2.2.5-2.0
- fix setup.py for Python 2

* Wed Sep 16 2015 Guy Streeter <streeter@redhat.com> - 2.2.4-2.0
- exclude only the arches numcatl is not built on
- change a test because of changed libnuma behavior in Fedora 22
- use the py2_build/install macros

* Tue Sep 15 2015 Guy Streeter <streeter@redhat.com> - 2.2.3-2.0
- fix typo in %%check
- add and update license text

* Thu Sep 10 2015 Guy Streeter <streeter@redhat.com> - 2.2.2-2.0
- fix "make tests" to point to the right PYTHONPATH

* Tue Sep  8 2015 Guy Streeter <streeter@redhat.com> - 2.2.1-2.0
- strip the object file
- mark the COPYING file as %%license
- add %%check section, remove %%clean

* Thu Aug 27 2015 Guy Streeter <streeter@redhat.com> - 2.2-2.0
- Combining the specfiles to build to python 2/3 simultaneously

* Wed Jul 22 2015 Guy Streeter <streeter@redhat.com> - 2.1.5-2.0
- buildrequire python-tools for msgfmt

* Wed Jul 22 2015 Guy Streeter <streeter@redhat.com> - 2.1.4-2.10
- Fedora 22 has numctl 2.0.10

* Wed Jul 22 2015 Guy Streeter <streeter@redhat.com> - 2.1.4-2.0.9
- setup.py improvements for standalone builds

* Mon Jul 13 2015 Guy Streeter <streeter@redhat.com> - 2.1.3-2.0.9
- numactl-devel no longer exports numa_parse_bitmap

* Fri Jan 30 2015 Guy Streeter <streeter@redhat.com> - 2.1.2-2.0.9
- python-setuptools-devel not required

* Tue Aug  5 2014 Guy Streeter <streeter@redhat.com>
- python3 port

* Wed Apr 23 2014 Guy Streeter <streeter@redhat.com> - 2.1.1-2.0.9_2
- the correct libnuma is auto-required, no need to specify

* Tue Apr 08 2014 Guy Streeter <streeter@redhat.com> - 2.1.1-2.0.9_1
- libnuma package name

* Tue Apr 08 2014 Guy Streeter <streeter@redhat.com> - 2.1-2.0.9_1.2
- New ver-rel scheme <package version>-<numactl version>_<package release>

* Fri Mar 28 2014 Guy Streeter <streeter@redhat.cpm> - 2.0-2.4.1
- rpmlint and mock build fixes

* Thu Aug  1 2013 Guy Streeter <streeter@redhat.com> - 2.0-2.3
- get rid of libnuma_readme.py

* Wed Jul 31 2013 Guy Streeter <streeter@redhat.com> - 2.0-2.1
- remove some debug print

* Tue Jul 30 2013 Guy Streeter <streeter@redhat.com> - 2.0-2
- minor version bump

* Tue Jul 30 2013 Guy Streeter <streeter@redhat.com> - 2.0-1
- Do not change the minor version number. It should match numactl.

* Tue Jul 23 2013 Guy Streeter <streeter@redhat.com> - 2.1-0
- convert to use Cython

* Mon Oct 15 2012 Guy Streeter <streeter@redhat.com> - 2.0-1
- use the versioned .so file
- match the numactl package version

* Tue Sep 25 2012 Guy Streeter <streeter@redhat.com> - 1.4-1.1
- Initial build
