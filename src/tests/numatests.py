#!/usr/bin/python3

# -*- coding: utf-8 -*-

#
# Copyright (C) 2013-2017 Red Hat, Inc.
#    This copyrighted material is made available to anyone wishing to use,
#  modify, copy, or redistribute it subject to the terms and conditions of
#  the GNU General Public License v.2.
#
#    This application is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#    General Public License for more details.
#
# Authors:
#    Guy Streeter <guy.streeter@gmail.com>
#
import libnuma
import unittest
import os
import ctypes
import ctypes.util


class TestNuma(unittest.TestCase):

    def setUp(self):
        self.assertTrue(libnuma.Available())

    def test_MaxPossibleNodes(self):
        self.assertGreaterEqual(libnuma.MaxPossibleNode(), 2)

    def test_NumPossibleNodes(self):
        self.assertGreaterEqual(libnuma.NumPossibleNodes(), 2)

    def test_MaxNode(self):
        self.assertGreaterEqual(libnuma.MaxNode(), 0)

    def test_NumConfiguredNodes(self):
        self.assertGreater(libnuma.NumConfiguredNodes(), 0)

    def test_GetMemsAllowed(self):
        mems_allowed = libnuma.GetMemsAllowed()
        self.assertIsInstance(mems_allowed, libnuma.Bitmask)
        #self.assertIsInstance(mems_allowed._bitmask, libnuma._BitmaskStructureP)

    def test_NumConfiguredCpus(self):
        self.assertGreater(libnuma.NumConfiguredCpus(), 0)

    def test_AllNodes(self):
        self.assertIsInstance(libnuma.AllNodes, libnuma.Bitmask)
        self.assertTrue(libnuma.AllNodes)
        self.assertGreater(libnuma.AllNodes.size, 0)

    def test_NoNodes(self):
        self.assertIsInstance(libnuma.NoNodes, libnuma.Bitmask)

    def test_AllCpus(self):
        self.assertIsInstance(libnuma.AllCpus, libnuma.Bitmask)
        self.assertGreater(libnuma.AllCpus.size, 0)

    def test_NumTaskCpus(self):
        self.assertGreater(libnuma.NumTaskCpus(), 0)

    def test_NumTaskNodes(self):
        self.assertGreater(libnuma.NumTaskNodes(), 0)

    def test_ParseNodestring(self):
        self.assertIsInstance(libnuma.ParseNodestring('all'), libnuma.Bitmask)
        with self.assertRaises(libnuma.ParseError):
            libnuma.ParseNodestring('$')

    def test_ParseCpustring(self):
        self.assertIsInstance(libnuma.ParseCpustring('0'), libnuma.Bitmask)
        with self.assertRaises(libnuma.ParseError):
            libnuma.ParseCpustring('Z')

    def test_Bitmask(self):
        b = libnuma.Bitmask(nodestring='all')
        self.assertIsInstance(b, libnuma.Bitmask)
        self.assertIsInstance(libnuma.Bitmask(cpustring='0'), libnuma.Bitmask)
        a = b
        self.assertIsInstance(a, libnuma.Bitmask)
        self.assertTrue(a == b)
        del(b)
        del(a)

    def test_NodeSize(self):
        s, f = libnuma.NodeSize(0)
        self.assertGreater(s, 1)
        self.assertGreater(f, 0)
        self.assertGreaterEqual(s, f)

    def test_Preferred(self):
        self.assertGreaterEqual(libnuma.Preferred(), 0)

    def test_SetPreferred(self):
        libnuma.SetPreferred(-1)

    def test_GetInterleaveMask(self):
        self.assertIsInstance(libnuma.GetInterleaveMask(), libnuma.Bitmask)

    def test_SetInterleaveMask(self):
        libnuma.SetInterleaveMask(libnuma.NoNodes)

    def test_Bind(self):
        libnuma.Bind(libnuma.AllNodes)

    def test_SetLocalalloc(self):
        libnuma.SetLocalalloc()

    def test_SetMembind(self):
        libnuma.SetMembind(libnuma.GetMemsAllowed())

    def test_GetMembind(self):
        self.assertIsInstance(libnuma.GetMembind(), libnuma.Bitmask)

    def test_AllocOnnode(self):
        v = libnuma.AllocOnnode(1, 0)
        libnuma.Free(v, 1)

    def test_AllocInterleaved(self):
        v = libnuma.AllocInterleaved(1)
        libnuma.Free(v, 1)

    def test_AllocInterleavedSubset(self):
        v = libnuma.AllocInterleavedSubset(1, libnuma.AllNodes)
        libnuma.Free(v, 1)

    def test_Alloc(self):
        v = libnuma.Alloc(1)
        v = libnuma.Realloc(v, 1, 2)
        libnuma.Free(v, 2)

    def test_RunOnNode(self):
        libnuma.RunOnNode(0)

    def test_RunOnNodeMask(self):
        libnuma.RunOnNodeMask(libnuma.AllNodes)
        with self.assertRaises(OSError):
            libnuma.RunOnNodeMask(libnuma.NoNodes)

    def test_GetRunNodeMask(self):
        self.assertIsInstance(libnuma.GetRunNodeMask(), libnuma.Bitmask)

    def test_TonodeMemory(self):
        v = libnuma.Alloc(1)
        libnuma.TonodeMemory(v, 1, 0)
        libnuma.Free(v, 1)

    def test_TonodemaskMemory(self):
        v = libnuma.Alloc(1)
        libnuma.TonodemaskMemory(v, 1, libnuma.AllNodes)
        libnuma.Free(v, 1)

    def test_SetlocalMemory(self):
        v = libnuma.Alloc(1)
        libnuma.SetlocalMemory(v, 1)
        libnuma.Free(v, 1)

    def test_PoliceMemory(self):
        v = libnuma.Alloc(8197)
        libnuma.TonodeMemory(v, 1, 0)
        libnuma.PoliceMemory(v, 8197)
        libnuma.Free(v, 8197)

    def test_SetBindPolicy(self):
        libnuma.SetBindPolicy(1)
        libnuma.SetBindPolicy(0)

    def test_SetStrict(self):
        libnuma.SetStrict(1)
        libnuma.SetStrict(0)

    def test_Distance(self):
        self.assertEqual(libnuma.Distance(0, 0), 10)
        # On Fedora 22, numa_distance() no longer returns an error when
        # an invalid node number is supplied.
        # with self.assertRaises(OSError):
        #    print(libnuma.MaxNode(), libnuma.MaxPossibleNode())
        #    print(libnuma.Distance(0, libnuma.MaxNode() + 1))
        #    print(libnuma.Distance(0, libnuma.MaxPossibleNode() + 1))

    def test_SchedGetaffinity(self):
        self.assertIsInstance(libnuma.SchedGetaffinity(1), libnuma.Bitmask)

    def test_Schedsetaffinity(self):
        mask = libnuma.AllocateCpumask()  # empty
        with self.assertRaises(OSError):
            libnuma.SchedSetaffinity(os.getpid(), mask)
        libnuma.SchedSetaffinity(os.getpid(), libnuma.AllCpus)

    def test_NodeToCpus(self):
        self.assertIsInstance(libnuma.NodeToCpus(0), libnuma.Bitmask)
        with self.assertRaises(OSError):
            libnuma.NodeToCpus(libnuma.MaxNode() + 1)

    def test_NodeOfCpu(self):
        libnuma.NodeOfCpu(0)

    def test_AllocateNodemask(self):
        self.assertIsInstance(libnuma.AllocateNodemask(), libnuma.Bitmask)

    def test_BitmaskAlloc(self):
        self.assertIsInstance(libnuma.BitmaskAlloc(1), libnuma.Bitmask)

    def test_BitmaskClearall(self):
        bmp = libnuma.BitmaskAlloc(2)
        libnuma.BitmaskSetbit(bmp, 1)
        libnuma.BitmaskClearall(bmp)
        self.assertEqual(libnuma.BitmaskIsbitset(bmp, 1), 0)

    def test_BitmaskClearbit(self):
        bmp = libnuma.BitmaskAlloc(2)
        libnuma.BitmaskSetbit(bmp, 1)
        self.assertEqual(libnuma.BitmaskIsbitset(bmp, 1), 1)
        libnuma.BitmaskClearbit(bmp, 1)
        self.assertEqual(libnuma.BitmaskIsbitset(bmp, 1), 0)
        libnuma.BitmaskClearbit(bmp, 99)

    def test_BitmaskEqual(self):
        bmp1 = libnuma.BitmaskAlloc(2)
        bmp2 = libnuma.BitmaskAlloc(3)
        self.assertEqual(libnuma.BitmaskEqual(bmp1, bmp2), 1)
        libnuma.BitmaskSetbit(bmp1, 1)
        self.assertEqual(libnuma.BitmaskEqual(bmp1, bmp2), 0)

    def test_BitmaskIsbitset(self):
        bmp = libnuma.BitmaskAlloc(2)
        self.assertEqual(libnuma.BitmaskIsbitset(bmp, 1), 0)

    def test_BitmaskNbytes(self):
        bmp = libnuma.BitmaskAlloc(9)
        self.assertGreaterEqual(libnuma.BitmaskNbytes(bmp), 2)

    def test_BitmaskSetall(self):
        bmp = libnuma.BitmaskAlloc(2)
        libnuma.BitmaskSetall(bmp)
        self.assertEqual(libnuma.BitmaskIsbitset(bmp, 0), 1)

    def test_BitmaskSetbit(self):
        bmp = libnuma.BitmaskAlloc(2)
        libnuma.BitmaskSetbit(bmp, 1)
        self.assertEqual(libnuma.BitmaskIsbitset(bmp, 0), 0)
        self.assertEqual(libnuma.BitmaskIsbitset(bmp, 1), 1)
#    def test_Nodemask_t(self):
#        n1 = libnuma.nodemask_t()
#        del n1
#    def test_BitmaskToNodemask(self):
#        bmp = libnuma.BitmaskAlloc(2)
#        libnuma.BitmaskSetbit(bmp, 1)
#        nodemask = libnuma.BitmaskToNodeMask(bmp)
#        self.assertFalse(nodemask.isnull)
#    def test_NodemaskToBitmask(self):
#        nodemask = libnuma.Nodemask()
#        nodemask.set_array_entry_value(0, 1)
#        bmp = libnuma.NodemaskToBitmask(nodemask)
#        self.assertEqual(libnuma.BitmaskIsbitset(bmp, 0), 1)

    def test_CopyBitmask(self):
        bmp = libnuma.BitmaskAlloc(2)
        libnuma.BitmaskSetbit(bmp, 1)
        bmp2 = libnuma.CopyBitmask(bmp)
        self.assertIsInstance(bmp2, libnuma.Bitmask)
        self.assertEqual(libnuma.BitmaskIsbitset(bmp2, 1), 1)

    def test_BitmaskWeight(self):
        bmp = libnuma.BitmaskAlloc(2)
        libnuma.BitmaskSetbit(bmp, 0)
        libnuma.BitmaskSetbit(bmp, 1)
        self.assertEqual(libnuma.BitmaskWeight(bmp), 2)

    def test_MovePages(self):
        import ctypes
        libc = ctypes.CDLL(ctypes.util.find_library('c'), use_errno=True)
        libc.malloc.restype = ctypes.c_void_p
        libc.free.argtypes = [ctypes.c_void_p]
        ps = os.sysconf('SC_PAGESIZE')
        mn = libnuma.MaxNode()
        pages = []
        pages.append(libc.malloc(ps + 1))
        pages.append(libc.malloc(ps + 1))
        nodes = []
        nodes.append(mn)
        nodes.append(0)
        status = libnuma.MovePages(os.getpid(), pages, nodes, 0)
        libc.free(pages[0])
        libc.free(pages[1])
        self.assertEqual(status[0], mn)
        self.assertEqual(status[1], 0)

    def test_MigratePages(self):
        libnuma.MigratePages(os.getpid(), libnuma.NoNodes, libnuma.AllNodes)

    def test_ExitOnError(self):
        self.assertEqual(libnuma.GetExitOnError(), 0)
        libnuma.SetExitOnError(1)
        self.assertEqual(libnuma.GetExitOnError(), 1)
        libnuma.SetExitOnError(0)

    def test_ExitOnWarn(self):
        libnuma.SetExitOnWarn(1)
        self.assertEqual(libnuma.GetExitOnWarn(), 1)
        libnuma.SetExitOnWarn(0)
        self.assertEqual(libnuma.GetExitOnWarn(), 0)

if __name__ == '__main__':
    unittest.main(verbosity=2)
