# -*- coding: utf-8 -*-

#
# Copyright (C) 2015-2017 Red Hat, Inc.
#    This copyrighted material is made available to anyone wishing to use,
#  modify, copy, or redistribute it subject to the terms and conditions of
#  the GNU General Public License v.2.
#
#    This application is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#    General Public License for more details.
#
# Authors:
#    Guy Streeter <guy.streeter@gmail.com>
#
